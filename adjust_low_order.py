#%%
import pandas as pd
import shutil
import os
import re
import sys



fldr = os.getcwd() + '\\'
padrao = "[+-]?\\d+(?:\\.\\d+)?(?:[eE][+-]?\\d+)?"

tol = 0.001

#%%
poten_pot = fldr + 'poten.pot'

def readPOT(arq_pot):
    with open(arq_pot) as pot:
        txt_pot = pot.readlines()

    flag = False
    for line in txt_pot:
        if '.gdf' in line:
            flag = True
        elif flag == True:
            [xbody,ybody,zbody,phibody] = [float(i) for i in re.findall(padrao,line)]
            flag=False
    return [xbody,ybody,zbody,phibody]

def writePOT(arq_pot, Xpot, Ypot, Zpot, Phi):
    with open(arq_pot) as pot:
        txt_pot = pot.readlines()

    with open(arq_pot,'w') as pot:
        flag = False
        for line in txt_pot:
            
            if flag == False:
                pot.write(line)
            else:
                pot.write(f'{Xpot:f} {Ypot:f} {Zpot:f} {Phi:f}\n')
                flag=False
            
            if '.gdf' in line:
                flag = True

    return None

if __name__=='__main__':

    [XBODY, YBODY, ZBODY, PHI] = readPOT(poten_pot)

    print(f'ZBODY = {ZBODY:f}')

    #%%
    low_order_gdf = fldr + 'ship_low.gdf'
    arqGDF = pd.read_csv(low_order_gdf,sep = "\s+", skiprows=4, header=None, names=('x','y','z'))

    pos = arqGDF.z > -ZBODY - tol

    arqGDF.z[pos] = -ZBODY

    N = len(arqGDF.z[pos])

    print(f'{N:d} vértices ajustados')

    with open(low_order_gdf) as txt:
        head = [next(txt) for x in range(4)]

    #backup of original file
    shutil.copyfile(low_order_gdf, low_order_gdf + '_original')

    if len(sys.argv) == 2:
        delta_z = float(sys.argv[1])
        arqGDF.z = arqGDF.z - delta_z
        shutil.copyfile(poten_pot, poten_pot + '_original')
        print(f'ZBODY_new = {ZBODY+delta_z:f}')
        writePOT(poten_pot, XBODY, YBODY, ZBODY + delta_z, PHI)



    with open(low_order_gdf,'w') as txt:
        for h in head:
            txt.write(h)

    arqGDF.to_csv(low_order_gdf,mode='a',sep=' ',index=None,header=None,float_format='%.7f')