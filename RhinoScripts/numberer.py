import Rhino
import scriptcontext as sc
import rhinoscriptsyntax as rs


def Numberer():
    
    crntNum = 0
    if sc.sticky.has_key("MyNum"):
        crntNum = sc.sticky["MyNum"]
    
    gp = Rhino.Input.Custom.GetPoint()
    
    gp.AddOption("Reset")
    
    while True:
        
        result = gp.Get()
        if gp.CommandResult()!=Rhino.Commands.Result.Success:
            return
            
        if result == Rhino.Input.GetResult.Option:
            crntNum = 0
            sc.sticky["MyNum"] = 0
            continue
        else:
            pt = gp.Point()
            if pt:
                rs.AddTextDot(str(crntNum +1),pt)
                crntNum = crntNum+1
                sc.sticky["MyNum"] = crntNum
            
            else: return
    
    
Numberer()