import rhinoscriptsyntax as rs

rs.Command("_-SetActiveViewport Perspective _Enter")

objs = rs.AllObjects()

for obj in objs:
    print "Object identifier: ", obj
    rs.SelectObject(obj)

pts = rs.BoundingBox(objs)

x = []
y = []
z = []
for pt in pts:
    x.append(pt[0])
    y.append(pt[1])
    z.append(pt[2])

dx = .1
dy = .1

delta = .5

min_z =  min(z)+delta
min_y = min(y)-dy
max_y = max(y)+dy
min_x = min(x)-dx
max_x = max(x)+dx

crv1 = rs.AddCurve([[min_x, min_y, min_z],[min_x, max_y, min_z]], degree=1)
crv2 = rs.AddCurve([[max_x, min_y, min_z],[max_x, max_y, min_z]], degree=1)

plane = rs.AddLoftSrf([crv1,crv2],start=None, end=None, loft_type=0, simplify_method=0, value=0, closed=False)

inter = rs.IntersectBreps(plane, objs[0])

rs.UnselectAllObjects()

rs.SelectObject(inter)

points = rs.DivideCurve(inter, 9, True, True)


for i,pt in enumerate(points):
    if (i != 0) and (i != 9):
        rs.MirrorObject(pt, [0,0,0], [-1,0,0], copy=False)

rs.UnselectAllObjects()
rs.Command("_-SelPt")

objectIds = rs.SelectedObjects()

folder = rs.WorkingFolder()

print folder

with open(folder + "\points_keel.txt", "w") as file:
    for objectId in objectIds:
        point = rs.PointCoordinates(objectId)
        file.write(str(point[0]) + " " + str(point[1]) + " 0\n")


rs.Command("_-Exit N Enter")


