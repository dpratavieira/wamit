import rhinoscriptsyntax as rs
import math as m

def exporta_volumes():
    
#    rs.EnableRedraw(False)
    
    rs.UnselectAllObjects()
    layers = rs.LayerNames()
    #layers = ["casco"]
    
    Mass = []
    Volume = []
    Rho = []
    CX = []
    CY = []
    CZ = []
    Rhot = {}
    lr_calc = []
    lr_calc2 = []
    
    for lr in layers:
        if rs.IsLayerVisible(lr):
            lr_calc.append(lr)
            volume = []
            cX = []
            cY = []
            cZ = []
            mass = []
            obj_list = rs.ObjectsByLayer(lr, True)
            for obj in obj_list:
                massi = rs.GetUserText(obj, "mass")
                vol = rs.SurfaceVolume(obj)
                cen = rs.SurfaceVolumeCentroid(obj)            
                if vol:  
                    mass.append(massi)
                    volume.append(vol[0])
                    cX.append(cen[0][0])
                    cY.append(cen[0][1])
                    cZ.append(cen[0][2])
            
            rs.UnselectAllObjects()
            print lr
            Mass.append(float(mass[0]))
            Volume.append(sum(volume))
            Rho.append(Mass[-1]/Volume[-1])
            Rhot[lr] = Mass[-1]/Volume[-1]
            #print "teste",Rhot[lr]
            CX.append(sum(mult(volume,cX)) / sum(volume))
            CY.append(sum(mult(volume,cY)) / sum(volume))
            CZ.append(sum(mult(volume,cZ)) / sum(volume))
            print "Layer:", lr
            print "  Massa: {:.2f}".format(Mass[-1])
            print "  Volume: {:.5f} m^3".format(Volume[-1])
            print "  Rho: {:.2f} kg/ m^3".format(Rho[-1])
            print "  CG: [{:.4f}, {:.4f}, {:.4f}] m".format(CX[-1], CY[-1], CZ[-1])
            #rs.AddPoint([CX[-1], CY[-1], CZ[-1]])
        
    Mass_t = sum(Mass)
    Vol_t = sum(Volume)
    cx = sum(mult(Mass,CX)) / sum(Mass)
    cy = sum(mult(Mass,CY)) / sum(Mass)
    cz = sum(mult(Mass,CZ)) / sum(Mass)
#    
    print "***************"
    print "Total:"
    print "  Massa: {:.2f} kg".format(Mass_t)
#    print "  Volume: {:.3f}".format(Vol_t)
    print "  CG: [{:.4f}, {:.4f}, {:.4f}] m".format(cx, cy, cz)
    
    objs = rs.AllObjects()
    rs.MoveObjects(objs,[-cx,-cy,-cz])
    rs.UnselectAllObjects()
    
    Prod = []
    Mom = []
    
    for lr in layers:
        if rs.IsLayerVisible(lr):
            lr_calc2.append(lr)
            #print lr
            #print Rhot[lr]
            rhoi = Rhot[lr]
            obj_list = rs.ObjectsByLayer(lr, True)
            ProdIn = []
            MomIn = []
            for obj in obj_list:
                massprop = rs.SurfaceVolumeMoments(obj)
                ProdIn.append(massprop[4])
                MomIn.append(massprop[6])
                #print massprop[6]
            
            Prod.append(mult_c(rhoi, soma_vetor(ProdIn)))
            Mom.append(mult_c(rhoi, soma_vetor(MomIn)))
            rs.UnselectAllObjects()
    
#    rs.EnableRedraw(True)
    
    Prod = soma_vetor(Prod)
    Mom = soma_vetor(Mom)
    
    objs = rs.AllObjects()
    rs.MoveObjects(objs,[cx,cy,cz])
    rs.UnselectAllObjects()
#    rs.AddPoint([cx,cy,cz])
    
    return [Prod,Mom]
    
def mult(a,b):
    multi = []
    for ai,bi in zip(a,b):
        multi.append(ai*bi)
    
    return multi
    
def mult_c(a,b):
    c = []
    for bi in b:
        c.append(a*bi)
    
    return c
    
def soma_vetor(a):
    sumx = 0
    sumy = 0
    sumz = 0
    for ai in a:
        sumx += ai[0]
        sumy += ai[1]
        sumz += ai[2]
    
    return [sumx,sumy,sumz]

#volume_python_rhino():
[Prod,Mom] = exporta_volumes()
print "[Ixy,Ixz,Iyz] = [{:.3f}, {:.3f}, {:.3f}] kg.m^2".format(Prod[0], Prod[1], Prod[2])
print "[Ixx,Iyy,Izz] = [{:.3f}, {:.3f}, {:.3f}] kg.m^2".format(Mom[0], Mom[1], Mom[2])