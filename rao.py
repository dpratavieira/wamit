#%%
import numpy as np
import scipy

def load_raos(wamit_folder):
    print('Carregando RAO:')
    print(f'   {wamit_folder}/force.4')
    arq4 = np.loadtxt(f'{wamit_folder}/force.4')

    angles_i = np.unique(arq4[:,1])
    dof_i = np.unique(arq4[:,2])
    Nmov = dof_i[-1]

    per_i = arq4[(arq4[:,2] == dof_i[0]) & (arq4[:,1] == angles_i[0]), 0]

    per = arq4[:,0]
    inc = arq4[:,1]
    dof = arq4[:,2]

    # The Wamit with 2 bodies shows lid (3 bodies)
    if Nmov > 12:
        Nmov = 12
    rao = arq4[:,5] + 1j * arq4[:,6]

    # Adjust ULEN
    ULEN = 1
    with open(f"{wamit_folder}/poten.pot") as f:
        poten = f.readlines()

    gdf_name=[]
    for pot in poten:
        if ".gdf" in pot:
            gdf_name.append(pot)

    gdf_name = gdf_name[0].replace('\n','')
    print(f"   GDF file: {gdf_name}")
    
    with open(f"{wamit_folder}/{gdf_name}", 'r') as f:
        gdf_file = f.readlines()
    
    ULEN = float(gdf_file[1].split()[0])
    print(f'   ULEN = {ULEN}')

    lines = np.isin(dof, [4, 5, 6, 10, 11, 12])
    rao[lines] /= ULEN

    rao_aux = []
    for gdl in dof_i:
        rao_aux.append(rao[dof == gdl].reshape((len(per_i),len(angles_i))))

    def rao_func(mov, incid):
        # inc_q = np.repeat(np.mod(incid+720, 360),len(per_i))
        per_g, inc_g = np.meshgrid(per_i, incid, indexing='ij')
        interp = scipy.interpolate.RegularGridInterpolator((per_i, angles_i), rao_aux[mov])

        return interp((per_g, inc_g))

    return rao_aux, per_i, angles_i, Nmov, rao_func


#%% debug
wamit = "C:\\Users\\daniel.prata\\OneDrive\\01_Projetos\\2023_TABR_VPORTS\\Analise-UKC\\HandymaxLNG\\HandymaxLNG_L183_B32.0_T08.00_D13m"

rao_aux, per_i, angles_i, Nmov, rao_func = load_raos(wamit_folder=wamit)

#%%
import matplotlib.pyplot as plt

plt.plot(per_i, np.abs(rao_func(3,[15,90])));
plt.xlim((0,30))
plt.grid()
plt.xlabel('T [s]')
plt.ylabel('RAO')

# %%
